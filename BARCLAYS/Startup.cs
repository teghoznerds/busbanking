﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BARCLAYS.Startup))]
namespace BARCLAYS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
