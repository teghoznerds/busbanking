﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using BARCLAYS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Security.Principal;
using BARCLAYS.Controllers;
using NLog;
using System.Threading.Tasks;
using System.Security.Policy;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace BARCLAYS.Models
{
    public class Utilities
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public HttpBrowserCapabilities bc = new HttpBrowserCapabilities();
        private ApplicationDbContext db = new ApplicationDbContext();
        public HttpContext hc;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public string GetUserIP(HttpRequestBase Request)
        {
            string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return Request.ServerVariables["REMOTE_ADDR"];
        }
        public string GetUserAgent(HttpRequestBase Request)
        {
            var userAgent = Request.UserAgent;
            return userAgent;
        }
        public void GetContext(AccountController ac, Controller c)
        {
            ac.ControllerContext = c.ControllerContext;
        }
        public ApplicationUser GetUser(IPrincipal User)
        {

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());

            if (currentUser != null)
            {
                return currentUser;
            }

            return null;

        }
        public ApplicationUser GetUser(ApplicationUser User)
        {

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Id);

            if (currentUser != null)
            {
                return currentUser;
            }

            return null;

        }
        public void InsertException(string action, DateTime date, string IP, string remarks, int? userId, int? contactId, string exception, string innerException)
        {
            ExceptionStore exs = new ExceptionStore
            {
                Action = action,
                Date = date,
                IP = IP,
                Remarks = remarks,
                ClientAccountId = contactId,
                BARCLAYSUserId = userId,
                Exception = exception,
                InnerException = innerException,
            };

            db.ExceptionStore.Add(exs);
            db.SaveChanges();
        }
        public void InsertAudit(string action, string browser, DateTime date, string IP, int? userId, int? contactId, string remarks, string status)
        {
            Audit app = new Audit
            {
                Action = action,
                Browser = browser,
                ActionStatus = status,
                Date = date,
                IP = IP,
                ContactAccountId = contactId,
                BARCLAYSUserId = userId,
                Remarks = remarks
            };

            db.Audit.Add(app);
            db.SaveChanges();

        }
        public void EmitLogger(string level, string message)
        {
            switch (level)
            {
                case "Trace":
                    try
                    {
                        logger.Log(LogLevel.Trace, message);
                    }
                    catch (Exception e)
                    {
                        e.Message.ToString();
                    }
                    break;
                case "Debug":
                    try
                    {
                        logger.Log(LogLevel.Debug, message);
                    }
                    catch (Exception e)
                    {
                        e.Message.ToString();
                    }

                    break;
                case "Info":
                    try
                    {
                        logger.Log(LogLevel.Info, message);
                    }
                    catch (Exception e)
                    {
                        e.Message.ToString();
                    }

                    break;
                case "Warn":
                    try
                    {
                        logger.Log(LogLevel.Warn, message);
                    }
                    catch (Exception e)
                    {
                        e.Message.ToString();
                    }

                    break;
                case "Error":
                    try
                    {
                        logger.Log(LogLevel.Error, message);
                    }
                    catch (Exception e)
                    {
                        e.Message.ToString();
                    }
                    break;
                case "Fatal":
                    try
                    {
                        logger.Log(LogLevel.Fatal, message);
                    }
                    catch (Exception e)
                    {
                        e.Message.ToString();
                    }
                    break;
            }
        }
        public string GetCurrencyIcon(string currency)
        {
            switch (currency)
            {
                case "USD":
                    return "<i class='fa fa-usd'></i>";
                case "GBP":
                    return "<i class='fa fa-gbp'></i>";
                case "YEN":
                    return "<i class='fa fa-jpy'></i>";
                case "RUPEES":
                    return "<i class='fa fa-inr'></i>";
                case "EURO":
                    return "<i class='fa fa-eur'></i>";
                default:
                    return "<i class='fa fa-money'></i>";
            }
        }
        public string GetWorkName(string projectTaskName, string taskName, string projectName)
        {
            string name = "";
            if (string.IsNullOrEmpty(projectTaskName) && string.IsNullOrEmpty(taskName))
            {
                name = projectName;
            }

            if (string.IsNullOrEmpty(projectTaskName) && string.IsNullOrEmpty(projectName))
            {
                name = taskName;
            }

            if (string.IsNullOrEmpty(taskName) && string.IsNullOrEmpty(projectName))
            {
                name = projectTaskName;
            }

            return name;
        }
        public BARCLAYSUsers GetUsers(string email)
        {
            return db.BARCLAYSUsers.Where(e => e.Email == email).FirstOrDefault();
        }
    }
}