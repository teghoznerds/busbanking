﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Collections.Generic;
using KALEXIUS.Models;

namespace KALEXIUS
{
    [HubName("applicationTicker")]
    public class ApplicationTickerHub : Hub
    {
        private readonly ApplicationTicker _applicationTicker;

        public ApplicationTickerHub() :
            this(ApplicationTicker.Instance)
        {

        }

        public ApplicationTickerHub(ApplicationTicker stockTicker)
        {
            _applicationTicker = stockTicker;
        }

        public IEnumerable<Application> GetAllApplications()
        {
            return _applicationTicker.GetAllApplications();
        }
    }
}