﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.SignalR.Hubs;
using TableDependency.EventArgs;
using TableDependency.Enums;
using System.Configuration;
using System.Data.SqlClient;
using TableDependency.SqlClient;
using TableDependency.Mappers;
using Microsoft.AspNet.SignalR;
using KALEXIUS.Models;
using KALEXIUS;

namespace LoanSystem
{
    public class ApplicationTicker
    {
        // Singleton instance
        private readonly static Lazy<ApplicationTicker> _instance = new Lazy<ApplicationTicker>(
            () => new ApplicationTicker(GlobalHost.ConnectionManager.GetHubContext<ApplicationTickerHub>().Clients));

        private static SqlTableDependency<Application> _tableDependency;

        private ApplicationTicker(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;

            //var mapper = new ModelToTableMapper<Application>();
            //mapper.AddMapping(s => s.AdminApproved, "Code");

            _tableDependency = new SqlTableDependency<Application>(
                ConfigurationManager.ConnectionStrings["LoanContext"].ConnectionString,
                "Applications");
            
            _tableDependency.OnChanged += SqlTableDependency_Changed;
            _tableDependency.OnError += SqlTableDependency_OnError;
            _tableDependency.Start();
        }

        public static ApplicationTicker Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }

        public IEnumerable<Application> GetAllApplications()
        {
            var ApplicationModel = new List<Application>();

            var connectionString = ConfigurationManager.ConnectionStrings["LoanContext"].ConnectionString;
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "SELECT * FROM [Applications]";

                    using (var sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            //var code = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Code"));
                            //var name = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Name"));
                            //var price = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Price"));

                            //ApplicationModel.Add(new Application { Symbol = code, Name = name, Price = price });
                            var rrr = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ApplicationId"));
                            var EEE = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AccountantApproved"));

                            var appl = new Application
                            {
                                AccountantApproved = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AccountantApproved")),
                                AdminApproved = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AdminApproved")),
                                SecretaryApproved = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("SecretaryApproved")),
                                LoanCommitteApproved = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("LoanCommitteApproved")),
                                ApplicantsId = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ApplicantsId")),
                                ApplicationSubmitted = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ApplicationSubmitted")),
                                ApplicationId = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ApplicationId")),
                                DateCreated = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("DateCreated")),
                                //Id = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Id")),
                                LastModified = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("LastModified")),
                                //LoanDisburseDate = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("LoanDisburseDate")),
                                LoanClosed = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("LoanClosed")),
                                LoanDisbursed = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("LoanDisbursed")),
                                IdCurrentStatus = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("IdCurrentStatus")),
                                ProjectLink = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ProjectLink")),
                                Rejected = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Rejected")),
                                Tsncs = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Tsncs")),
                            };

                            ApplicationModel.Add(appl);
                        }
                    }
                }
            }

            return ApplicationModel;
        }

        void SqlTableDependency_OnError(object sender, ErrorEventArgs e)
        {
            throw e.Error;
        }

        /// <summary>
        /// Broadcast New Application Price
        /// </summary>
        void SqlTableDependency_Changed(object sender, RecordChangedEventArgs<Application> e)
        {
            if (e.ChangeType != ChangeType.None)
            {
                BroadcastApplicationPrice(e.Entity);
            }
        }

        private void BroadcastApplicationPrice(Application Application)
        {
            Clients.All.updateApplicationPrice(Application);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _tableDependency.Stop();
                }

                disposedValue = true;
            }
        }

        ~ApplicationTicker()
        {
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}