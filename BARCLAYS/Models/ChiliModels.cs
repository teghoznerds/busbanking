﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Collections;

namespace BARCLAYS.Models
{
    public enum UserTypes
    {
        Admin,
        Customer
    }
    public class BARCLAYSUsers
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public string Username { get; set; }
        public string UserId { get; set; }
        public bool isActive { get; set; }
        public UserTypes UserTypes { get; set; }
        public DateTime DOB { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }
    }
    public class BARCLAYSAdmin
    {
        public int Id { get; set; }
        public int BARCLAYSUserId { get; set; }
        [ForeignKey("BARCLAYSUserId")]
        public virtual BARCLAYSUsers BARCLAYSUsers { get; set; }
    }
    public class Customer
    {
        public int Id { get; set; }
        public int BARCLAYSUserId { get; set; }
        [ForeignKey("BARCLAYSUserId")]
        public virtual BARCLAYSUsers BARCLAYSUsers { get; set; }
    }
    public class Audit
    {
        public int Id { get; set; }
        public string Browser { get; set; }
        public string IP { get; set; }
        public DateTime? Date { get; set; }
        public string Action { get; set; }
        public string ActionStatus { get; set; }
        public int? ContactAccountId { get; set; }
        public int? BARCLAYSUserId { get; set; }
        public string Remarks { get; set; }
        [ForeignKey("BARCLAYSUserId")]
        public BARCLAYSUsers BARCLAYSUsers { get; set; }
    }
    public class ExceptionStore
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string IP { get; set; }
        public string Action { get; set; }
        public string Exception { get; set; }
        public string InnerException { get; set; }
        public int? ClientAccountId { get; set; }
        public int? BARCLAYSUserId { get; set; }
        public string Remarks { get; set; }
        [ForeignKey("BARCLAYSUserId")]
        public BARCLAYSUsers BARCLAYSUsers { get; set; }
    }
    public class Packages
    {

    }
    public class ComplainTickets
    {

    }
    public class Feedbacks
    {

    }
}