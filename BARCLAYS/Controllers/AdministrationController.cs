﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BARCLAYS.Models;

namespace BARCLAYS.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdministrationController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        //
        // GET: /Administration/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Analytics()
        {
            return View();
        }

        #region Masters
        public ActionResult Currency()
        {
            return View();
        }
        #region clients
        public ActionResult Clients()
        {
            return View();
        }
        #endregion       

        public ActionResult BillUnit()
        {
            return View();
        }
        public ActionResult BillCode()
        {
            return View();
        }
        public ActionResult Employee()
        {
            return View();
        }

        public ActionResult Client()
        {
            return View();
        }
        #endregion

        //
        // GET: /Administration/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Administration/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Administration/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Administration/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Administration/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Administration/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Administration/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
