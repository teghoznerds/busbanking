﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BARCLAYS.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;

namespace BARCLAYS.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ExceptionStoresController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ExceptionStores1
        public ActionResult Index()
        {
            ViewBag.AppAuditList = GetAppAudit();
            return View();
        }

        public IList<Audit> GetAppAuditx()
        {
            List<Audit> ega = new List<Audit>();

            foreach (Audit e in db.Audit.ToList())
            {
                ega.Add(e);
            }
            return ega;
        }

        public List<Audit> GetAppAudit()
        {
            var Query = (from a in db.Audit
                         orderby a.Date descending
                         select a).ToList();

            return Query;
        }

        public JsonResult GetAppAudits([DataSourceRequest] DataSourceRequest request)
        {
            var finalTasks = GetAppAudit();
            var forKendo = new { total = finalTasks.ToList().Count(), data = finalTasks.ToDataSourceResult(request) };
            var william = JsonConvert.SerializeObject(forKendo);
            return Json(william, JsonRequestBehavior.AllowGet);
        }

        // GET: ExceptionStores1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExceptionStore exceptionStore = db.ExceptionStore.Find(id);
            if (exceptionStore == null)
            {
                return HttpNotFound();
            }
            return View(exceptionStore);
        }

        // GET: ExceptionStores1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ExceptionStores1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( ExceptionStore exceptionStore)
        {
            if (ModelState.IsValid)
            {
                db.ExceptionStore.Add(exceptionStore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(exceptionStore);
        }

        // GET: ExceptionStores1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExceptionStore exceptionStore = db.ExceptionStore.Find(id);
            if (exceptionStore == null)
            {
                return HttpNotFound();
            }
            return View(exceptionStore);
        }

        // POST: ExceptionStores1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,date,time,IP,action,user,message,browser")] ExceptionStore exceptionStore)
        {
            if (ModelState.IsValid)
            {
                db.Entry(exceptionStore).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(exceptionStore);
        }

        // GET: ExceptionStores1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExceptionStore exceptionStore = db.ExceptionStore.Find(id);
            if (exceptionStore == null)
            {
                return HttpNotFound();
            }
            return View(exceptionStore);
        }

        // POST: ExceptionStores1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ExceptionStore exceptionStore = db.ExceptionStore.Find(id);
            db.ExceptionStore.Remove(exceptionStore);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
