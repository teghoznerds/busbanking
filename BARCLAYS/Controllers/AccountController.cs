﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using BARCLAYS.Models;
using System.Collections.Generic;
using RazorEngine;
using RazorEngine.Templating;
using Postal;
using System.IO;

namespace BARCLAYS.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        Utilities u = new Utilities();
        EmailsController ec = new EmailsController();

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string status)
        {
            if (!string.IsNullOrEmpty(status))
            {
                ModelState.AddModelError("", "Account is Inactive. Please contact Administrator");
            }

            //if (User.Identity.IsAuthenticated)
            //{
            //    if (User.IsInRole("Admin"))
            //    {
            //        return RedirectToAction("Index", "Administration");
            //    }
            //    //else
            //    //{
            //    //    return RedirectToAction("Index", "TimeSheets");
            //    //}
            //}
            
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            int? _employeeId = null;
            int? _contactId = null;

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model State is Invalid.");
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var newRememberMe = model.RememberMe == "on" ? true : false ;
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, newRememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                    //var user = await UserManager.FindAsync(model.Email, model.Password);
                    //var roles = await UserManager.GetRolesAsync(user.Id);
                    //var currentUser = u.GetUser(user);

                    //if (currentUser != null)
                    //{
                    //    string _logMessage = "Employee(" + currentUser.FirstName.ToString() + " " + currentUser.LastName.ToString() + ") Logged in ";
                    //    u.EmitLogger("Info", _logMessage);

                    //    string _action = "Employee Login";
                    //    string _remarks = "Employee(" + currentUser.FirstName.ToString() + " " + currentUser.LastName.ToString() + ") Logged in ";
                    //    string _status = "Success";
                    //    string _browser = u.GetUserAgent(Request);
                    //    DateTime _date = DateTime.Now.Date;
                    //    string _time = DateTime.Now.TimeOfDay.ToString();
                    //    string _IP = u.GetUserIP(Request);
                    //    _employeeId = user != null ? u.GetUser(user).EmployeeId : null;
                    //    _contactId = user != null ? u.GetUser(user).ClientAccountId : null;
                    //    string _userr = currentUser.FirstName.ToString() + " " + currentUser.LastName.ToString();
                    //    u.InsertAudit(_action, _browser, _date, _IP, _employeeId, _contactId, _remarks, _status);
                    //}


                    //user.isFirstLogin = 1;                                       

                    //if (user.IsActive == true)
                    //{
                    //    if (roles.Count == 1)
                    //    {
                    //        if (roles.Contains("Employee"))
                    //        {
                    //            return RedirectToAction("Index", "Administration");
                    //        }
                    //        if (roles.Contains("Supervisor"))
                    //        {
                    //            return RedirectToAction("Index", "Administration");
                    //        }
                    //        if (roles.Contains("Admin"))
                    //        {
                    //            return RedirectToAction("Index", "Administration");
                    //        }
                    //    }
                    //    if (roles.Count > 1)
                    //    {
                    //        if (roles.Contains("Supervisor") || roles.Contains("Employee") || !roles.Contains("Admin"))
                    //        {
                    //            return RedirectToAction("Index", "Administration");
                    //        }
                    //    }
                    //    else
                    //    {
                    //        return RedirectToLocal(returnUrl);
                    //    }                        
                    //}
                    //else
                    //{
                    //    ModelState.AddModelError("", "Account is Inactive. Please contact Administrator.");
                    //    string logMessage = "Inactive Employee(" + u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString() + ") Account Logged in ";
                    //    u.EmitLogger("Info", logMessage);

                    //    string action = "inactive Employee Login";
                    //    string remarks = "Inactive Employee(" + u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString() + ") Account Logged in ";
                    //    string status = "Success";
                    //    string browser = u.GetUserAgent(Request);
                    //    DateTime date = DateTime.Now.Date;
                    //    string time = DateTime.Now.TimeOfDay.ToString();
                    //    string IP = u.GetUserIP(Request);
                    //    string userr = u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString();
                    //    _employeeId = user != null ? u.GetUser(user).EmployeeId : null;
                    //    _contactId = user != null ? u.GetUser(user).ClientAccountId : null;
                    //    u.InsertAudit(action, browser, date, IP, _employeeId, _contactId, remarks, status);
                    //}
                    var message = "Failed.";
                        //ModelState.AddModelError("", "You have some Priviledge Challenges.");
                        //string logMessagex = "Employee(" + u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString() + ") Log in attempt failed";
                        //u.EmitLogger("Error", logMessagex);

                        //string actionx = "Employee Login failure";
                        //string remarksx = "Employee(" + u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString() + ") Log in attempt failed ";
                        //string statusx = "Failure";
                        //string browserx = u.GetUserAgent(Request);
                        //DateTime datex = DateTime.Now.Date;
                        //string timex = DateTime.Now.TimeOfDay.ToString();
                        //string IPx = u.GetUserIP(Request);
                        //string userrx = u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString();
                        //_employeeId = user != null ? u.GetUser(user).EmployeeId : null;
                        //_contactId = user != null ? u.GetUser(user).ClientAccountId : null;
                        //u.InsertAudit(actionx, browserx, datex, IPx, _employeeId, _contactId, remarksx, statusx);
                        return RedirectToAction("Login", new {status= message});
                case SignInStatus.LockedOut:
                        string logMessagexxx = "Employee(" + u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString() + ") Log in Account Locked";
                        u.EmitLogger("Error", logMessagexxx);
                        string actionxxx = "Employee Login Account Locked";
                        string remarksxxx = "Employee(" + u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString() + ") Log in Account Locked ";
                        string statusxxx = "LockedOut";
                        string browserxxx = u.GetUserAgent(Request);
                        DateTime datexxx = DateTime.Now.Date;
                        string timexxx = DateTime.Now.TimeOfDay.ToString();
                        string IPxxx = u.GetUserIP(Request);
                        string userrxxx = u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString();
                        _employeeId = u.GetUser(User).BARCLAYSUserId;
                        u.InsertAudit(actionxxx, browserxxx, datexxx, IPxxx, _employeeId, _contactId, remarksxxx, statusxxx);
                        return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:

                    string logMessagexx = "Employee(" + u.GetUsers(model.Email) + ") Log in invalid";
                    u.EmitLogger("Error", logMessagexx);

                    string actionxx = "Employee Login Invalid";
                    string remarksxx = "Employee(" + u.GetUsers(model.Email) + ") Log in invalid ";
                    string statusxx = "Invalid";
                    string browserxx = u.GetUserAgent(Request);
                    DateTime datexx = DateTime.Now.Date;
                    string timexx = DateTime.Now.TimeOfDay.ToString();
                    string IPxx = u.GetUserIP(Request);
                    _employeeId = u.GetUser(User).BARCLAYSUserId;
                    u.InsertAudit(actionxx, browserxx, datexx, IPxx, _employeeId, _contactId, remarksxx, statusxx);
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult CustomerRegistration(string returnUrl, string status)
        {
            if (!string.IsNullOrEmpty(status))
            {
                ModelState.AddModelError("", "Account is Inactive. Please contact Administrator");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Register(RegisterViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
        //        var result = await UserManager.CreateAsync(user, model.Password);
        //        if (result.Succeeded)
        //        {
        //            await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);
                    
        //            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
        //            // Send an email with this link
        //            // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
        //            // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
        //            // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

        //            return RedirectToAction("Index", "Admin");
        //        }
        //        AddErrors(result);
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        public void CheckEmitUser(ApplicationDbContext db, int employeeId, int isAdministrator, int iSupervisor, int isPartner)
        {

            string email = db.BARCLAYSUsers.AsNoTracking().Where(e => e.Id == employeeId).FirstOrDefault().Email;
            var userr = UserManager.FindByEmail(email);
            IList<string> UseRoles;
            UseRoles = UserManager.GetRoles(userr.Id);
            if (isAdministrator == 0 && UseRoles.Contains("Admin"))
            {
                UserManager.RemoveFromRole(userr.Id, "Admin");
            }
            if (iSupervisor == 0 && UseRoles.Contains("Supervisor"))
            {
                UserManager.RemoveFromRole(userr.Id, "Supervisor");
            }
            if (isPartner == 0 && UseRoles.Contains("Partner"))
            {
                UserManager.RemoveFromRole(userr.Id, "Partner");
            } 
        }

        public void CheckEmitUser(string email, int isAdministrator, int iSupervisor, int isPartner)
        {
            var userr = UserManager.FindByEmail(email);
            IList<string> UseRoles;
            UseRoles = UserManager.GetRoles(userr.Id);
            if (isAdministrator == 0 && UseRoles.Contains("Admin"))
            {
                UserManager.RemoveFromRole(userr.Id, "Admin");
            }
            if (iSupervisor == 0 && UseRoles.Contains("Supervisor"))
            {
                UserManager.RemoveFromRole(userr.Id, "Supervisor");
            }
            if (isPartner == 0 && UseRoles.Contains("Partner"))
            {
                UserManager.RemoveFromRole(userr.Id, "Partner");
            }
        }

        public void CreateRole(ApplicationDbContext db, int UsersId, int isAdministrator, int iSupervisor, int isPartner)
        {
            string email = db.BARCLAYSUsers.AsNoTracking().Where(e => e.Id == UsersId).FirstOrDefault().Email;
            var userr = UserManager.FindByEmail(email);
            IList<string> UseRoles;
            UseRoles = UserManager.GetRoles(userr.Id);

            if (isAdministrator == 1 && !UseRoles.Contains("Admin"))
            {
                UserManager.AddToRole(userr.Id, "Admin");
            }
            if (iSupervisor == 1 && !UseRoles.Contains("Supervisor"))
            {
                UserManager.AddToRole(userr.Id, "Supervisor");
            }
            if (isPartner == 1 && !UseRoles.Contains("Partner"))
            {
                UserManager.AddToRole(userr.Id, "Partner");
            }
        }

        public void CreateRole(string email, int isAdministrator, int iSupervisor, int isPartner)
        {
            var userr = UserManager.FindByEmail(email);
            IList<string> UseRoles;
            UseRoles = UserManager.GetRoles(userr.Id);

            if (isAdministrator == 1 && !UseRoles.Contains("Admin"))
            {
                UserManager.AddToRole(userr.Id, "Admin");
            }
            if (iSupervisor == 1 && !UseRoles.Contains("Supervisor"))
            {
                UserManager.AddToRole(userr.Id, "Supervisor");
            }
            if (isPartner == 1 && !UseRoles.Contains("Partner"))
            {
                UserManager.AddToRole(userr.Id, "Partner");
            } 
        }


        //public async Task<RegistrationFeedback> RegisterEmitUser(Employees model)
        //{
        //    if (ModelState.IsValid)
        //    {
                
        //        var user = new ApplicationUser()
        //        {
        //            UserName = model.EmployeeEmail,
        //            EmployeeId = model.EmployeeId,
        //            Email = model.EmployeeEmail,
        //            FirstName = model.EmployeeFirstName,
        //            LastName = model.EmployeeLastName,
        //            isActive = 1,
        //            isFirstLogin = 0,
        //            LockoutEnabled = true,
        //            LockoutEndDateUtc = DateTime.UtcNow.AddMinutes(15)                    
        //        };

        //        var userpassword = model.EmployeeId + "Em!T" + DateTime.Now.Year;
        //        IdentityResult result = UserManager.Create(user, userpassword);

        //        string subject = "";
        //        string body = "";
        //        //to add roles               
        //        //UserManager.AddToRole(user.Id, "Admin");
        //        string from = "info@infinigroup.biz";

        //        if (result.Succeeded)
        //        {
        //            subject = user.FirstName + " " + user.LastName + ", an Account has been created for you on Emit";
        //            body = "Below is the Login Combination of your Account. Please Note on first time login a password change is required. <br> " +
        //                "Username: " + user.Email + "<br>" + "Password: " + userpassword;
                   
        //            string fname = user.FirstName + " " + user.LastName;
        //            //ec.SendMail(user.Email, from, subject, user.Email, userpassword, body, fname);                                    

        //            UserManager.AddToRole(user.Id, "Employee");

        //            if (model.isAdministrator > 0)
        //            {
        //                UserManager.AddToRole(user.Id, "Admin");
        //                //UserManager.AddToRole(user.Id, "Supervisor");                        
        //            }
        //            if (model.isSupervisor > 0)
        //            {
        //                UserManager.AddToRole(user.Id, "Supervisor");
        //                //u.SendEmitMail(user,UserManager,"","",);
        //            }                   

        //            return new RegistrationFeedback { UserId = user.Id, Password = userpassword };
        //        }
        //        else
        //        {
        //            AddErrors(result);
        //        }
        //    }
        //    // If we got this far, something failed
        //    return null;
        //}

        //public async Task SendEmployeeRegistrationEmail(string userId, string password, UrlHelper url, string companyName, string companyEmail)
        //{
        //    string subject = "";
        //    string body = "";
        //    string from = companyEmail ;

        //    ApplicationUser user = UserManager.FindById(userId);

        //    if(user != null)
        //    {
        //        subject = user.FirstName + " " + user.LastName + ", an Account has been created for you on Emit";

        //        body = "<hgroup class='title'><h1>Dear " + user.FirstName + " " + user.LastName + ", </h1></hgroup>" +
        //        "<p>Below is the Login Combination of your Account.</p>" +
        //        "<p>Please Note on first time login a password change is required.</p>" +
        //        "<p class='align-left'><h3>Username: " + user.Email + "</h3></p>" +
        //        "<p class='align-left'><h3>Password: " + password + "</h3></p>" +
        //        "<p class='align-left'>Welcome to Emit!</p>" +
        //        "<p class='align-left'>Regards,</p>" +
        //        "<p class='align-left'>" + companyName + "</p>";

        //        StreamReader reader = new StreamReader(Server.MapPath("~/Views/Shared/emailtemplate.html"));
        //        StreamReader readerStyle = new StreamReader(Server.MapPath("~/Views/Shared/emailstyle.txt"));
        //        string readFile = reader.ReadToEnd();
        //        string readFileStyle = readerStyle.ReadToEnd();
        //        string StrContent = "";
        //        StrContent = readFile;

        //        //Here replace the name with [MyName]
        //        StrContent = StrContent.Replace("[BODY]", body).Replace("[styles]", readFileStyle).Replace("[COMPANYNAME]", companyName);

        //        string fname = user.FirstName + " " + user.LastName;
        //        ec.SendMail(user.Email, from, subject, user.Email, password, StrContent, fname);
        //        await u.SendEmitMail(user, UserManager, subject, StrContent);
        //    }          
        //}

        //
        // GET: /Account/ConfirmEmail

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        
        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                //var user = await UserManager.FindByNameAsync(model.Email);
                var user = await UserManager.FindByEmailAsync(model.Email);
                var myUsers = UserManager.Users;

                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    //string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    //var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    ///await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    //await UserManager.SendSmsAsync(user.Id, "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    //string logMessagex = "Employee(" + model.Email + ") Requested for Password Change";
                    //u.EmitLogger("Error", logMessagex);

                    //string actionx = "Employee Password Change";
                    //string remarksx = "Employee(" + model.Email + ") Requested for password Change ";
                    //string statusx = "Success";
                    //string browserx = u.GetUserAgent(Request);
                    //DateTime datex = DateTime.Now.Date;
                    //string timex = DateTime.Now.TimeOfDay.ToString();
                    //string IPx = u.GetUserIP(Request);
                    //string userrx = model.Email;
                    //int? useridx = null;
                    //u.InsertAudit(actionx, browserx, datex, timex, IPx, userrx, useridx, remarksx, statusx);
                    return View("ForgotPasswordConfirmation");
                }

                if (!(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    //string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    //var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    //await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    ////await UserManager.SendSmsAsync(user.Id, "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    //string logMessagex = "Employee(" + u.GetEmployeeName(user.EmployeeId) + ") Requested for Password Change";
                    //u.EmitLogger("Error", logMessagex);

                    //string actionx = "Employee Password Change";
                    //string remarksx = "Employee(" + u.GetEmployeeName(user.EmployeeId) + ") Requested for password Change ";
                    //string statusx = "Success";
                    //string browserx = u.GetUserAgent(Request);
                    //DateTime datex = DateTime.Now.Date;
                    //string timex = DateTime.Now.TimeOfDay.ToString();
                    //string IPx = u.GetUserIP(Request);
                    //string userrx = u.GetEmployeeName(user.EmployeeId);
                    //int useridx = user.EmployeeId;
                    //u.InsertAudit(actionx, browserx, datex, timex, IPx, remarksx, userrx, useridx, statusx);
                    return View("ForgotPasswordConfirmation");
                }


                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                 //string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                 //var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                 //await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                 return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                //string logMessagex = "Employee(" + u.GetEmployeeName(user.EmployeeId) + ") Password Changed";
                //u.EmitLogger("Error", logMessagex);

                //string actionx = "Employee Password Change";
                //string remarksx = "Employee(" + u.GetEmployeeName(user.EmployeeId) + ") Password Changed ";
                //string statusx = "Success";
                //string browserx = u.GetUserAgent(Request);
                //DateTime datex = DateTime.Now.Date;
                //string timex = DateTime.Now.TimeOfDay.ToString();
                //string IPx = u.GetUserIP(Request);
                //string userrx = u.GetEmployeeName(user.EmployeeId);
                //string useridx = user.EmployeeId;
                //u.InsertAudit(actionx, browserx, datex, timex, IPx, remarksx, userrx, useridx, statusx);
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            //string logMessagex = "Employee(" + u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString() + ") Logged Off";
            //u.EmitLogger("Error", logMessagex);

            //string actionx = "Employee Log Out";
            //string remarksx = "Employee(" + u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString() + ") Logged Off ";
            //string statusx = "Success";
            //string browserx = u.GetUserAgent(Request);
            //DateTime datex = DateTime.Now.Date;
            //string timex = DateTime.Now.TimeOfDay.ToString();
            //string IPx = u.GetUserIP(Request);
            //string userrx = u.GetUser(User).FirstName.ToString() + " " + u.GetUser(User).LastName.ToString();
            //int userid = u.GetUser(User).EmployeeId;
            //u.InsertAudit(actionx, browserx, datex, timex, IPx, remarksx, userrx, userid, statusx);
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        public enum loginError
        {
            InvalidePassword = 1,

        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Administration");
        }

        private ActionResult RedirectToLocalEmit(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Administration");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}