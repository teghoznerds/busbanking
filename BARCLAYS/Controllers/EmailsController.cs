﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RazorEngine;
using Postal;
using BARCLAYS.Models;

namespace BARCLAYS.Controllers
{
    public class EmailsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public Utilities eu = new Utilities();
        //public TimeSheetsController ts = new TimeSheetsController();

        // GET: Mailer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MailTemplate()
        {
            dynamic email = new Email("Welcome");
            email.To = "aghogho@infinigroup.biz";
            email.From = "info@infinigroup.biz";
            email.Send();
            return View();
        }
        public ActionResult Mailz()
        {
            dynamic email = new Email("WelcomeTest");
            email.To = "aghogho@infinigroup.biz";
            email.From = "info@infinigroup.biz";
            email.Header = "Just to test";
            email.Send();
            return View();
        }

        public void SendMail(string to, string from, string header, string username, string password, string content, string name)
        {
            //try
            //{
            //    int globalMessaging = db.GlobalSettings.Where(t => t.Name == "AllowNotifications").FirstOrDefault().Value;
            //    int sendAdminsNotification = db.GlobalSettings.Where(t => t.Name == "EmailAdmins").FirstOrDefault().Value;
            //    int sendEmployeeEmail = db.GlobalSettings.Where(t => t.Name == "EmailEmployees").FirstOrDefault().Value;

            //    dynamic email = new Email("Welcome");

            //    if (globalMessaging != 0)
            //    {
            //        if (sendEmployeeEmail == 1)
            //        {
            //            email.To = to;
            //            email.From = from;
            //            email.Header = header;
            //            email.Username = username;
            //            email.Password = password;
            //            email.Name = name;
            //            email.Send();
            //        }               
            //    }
            //}
            //catch(Exception e){

            //}
                        
           
        }

        // GET: Mailer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Mailer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Mailer/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Mailer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Mailer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Mailer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Mailer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
