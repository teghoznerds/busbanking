$(function () {

    /**
     * Pie charts data and options used in many views
     */

    $("span.pie").peity("pie", {
        fill: ["#de2275  ", "#edf0f5"]
    })

    $(".line").peity("line",{
        fill: '#de2275  ',
        stroke:'#edf0f5',
    })

    $(".bar").peity("bar", {
        fill: ["#de2275  ", "#edf0f5"]
    })

    $(".bar_dashboard").peity("bar", {
        fill: ["#de2275  ", "#edf0f5"],
    })
});
