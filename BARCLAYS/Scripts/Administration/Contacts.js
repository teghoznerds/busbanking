﻿$(document).ready(function () {
    var Execution = (function () {
        return {
            Settings: {},
            Init: function () {
                console.log("Contact Initialized!!")
                var menu = this.Settings;
                this.BindUIAction(menu);
                this.datatableInit($("#datatable-ajax"));
                $('.datapicker').datepicker();
                this.HandleCheckContactType();
            },
            BindUIAction: function (menu) {
            },
            HandleCheckContactType: function () {
                $('.ctype').change(function () {
                    if ($(this).is(":checked")) {
                        //'checked' event code
                        Execution.HandleContactType($(this).data("attr"));
                        return;
                    }
                    //'unchecked' event code
                    Execution.HandleContactType($(this).data("attr"));
                });
            },
            HandleContactType: function (type) {
                console.log("type: ", type);
                switch (type) {
                    case "INDIVIDUAL":
                        $.each($(".corperate"), function () {
                            $(this).attr("readonly", "readonly");
                        });

                        $.each($(".personal"), function () {
                            $(this).removeAttr("readonly", "readonly");
                        });
                        break;
                    case "CORPORATE":
                        console.log("i entered");
                        $.each($(".personal"), function () {
                            $(this).attr("readonly", "readonly");
                        });

                        $.each($(".corperate"), function () {
                            $(this).removeAttr("readonly", "readonly");
                        });
                        break;
                }               
            },
            datatableInit: function (table) {
                var $table = table;

                //executable.inserTheHeader($table);
                var details = [];

                var datatable = $table.dataTable({
                    "processing": true,
                    "serverSide": false,
                    "ajax": {
                        "url": $table.data('url'),
                        //"data": { providerId: $table.data('param') },
                        "type": "POST",
                        "dataSrc": function (result) {
                            var result = JSON.parse(result);
                            //console.log("the data id: ", result.id)
                            console.log("the result: ", result)
                            return result.data;
                        }
                    },
                    "columns": [
                        {
                            "class": "text-center",
                            "orderable": false,
                            "data": null,
                            "defaultContent": '<i data-toggle class="fa fa-plus-square-o text-primary fa-2x" style="cursor: pointer;"></i>',
                        },
                        //{ data: "Id" },
                        //{
                        //    data: function (data) {
                        //        return data.Patient.FirstName + " " + data.Patient.LastName;
                        //    }
                        //},
                        //{ data: "MeetingPurpose" },                       
                        { data: "ContactType.Type" },
                        { data: "ContactCode" },
                        //{ data: "Comments" },
                        {
                            "class": "text-center",
                            data: function (data) {
                                var value = "";
                                if (data.Status == 0) {
                                    value = "<i class='fa fa-times-circle fa-2x' style='color: red;'></i>";
                                }
                                else {
                                    value = "<i class='fa fa-check-circle fa-2x' style='color: green;'></i>";
                                }

                                return value;
                            }
                        },
                        { data: "DateCreated" },
                        { data: "DateLastUpdated" },                       
                        {
                            "class": "text-center",
                            data: function (data) {
                                var value = "";
                                var edit = "<a class='btn btn-primary' href='/Administration/EditContact?ContactId="+ data.Id +"'><i class='fa fa-edit'></i></a> &nbsp;";
                                var del = "<a class='btn btn-danger' href='/Administration/DeleteAContact?ContactId=" + data.Id + "'><i class='fa fa-trash'></i></a>";
                                var allBtns = edit + del;

                                return allBtns;
                            },
                            
                        },
                        { data: "FullName", "visible": false },
                        { data: "CompanyName", "visible": false },
                        { data: "Email", "visible": false },
                        { data: "CompanyEmail", "visible": false },
                        { data: "Address", "visible": false },
                        { data: "CompanyAddress", "visible": false },
                        { data: "PhoneNumber", "visible": false },
                        { data: "CompanyPhone", "visible": false },
                        { data: "FullName", "visible": false },
                        { data: "MobileNumber", "visible": false },
                        { data: "CompanyRegistrationNumber", "visible": false },
                        { data: "DOB", "visible": false },
                        { data: "CompanyDateStarted", "visible": false },
                        { data: "NIDNumber", "visible": false },
                        { data: "Id", "visible": false },
                        { data: "PassportNumber", "visible": false },
                        { data: "CompanyFax", "visible": false },
                        { data: "CompanyContactPerson", "visible": false },
                        { data: "NextOFKin", "visible": false },
                        { data: "CompanyWebsite", "visible": false },
                        //{ data: "FullName", "visible": false },
                    ],
                    aoColumnDefs: [{
                        bSortable: false,
                        aTargets: [0],
                        //width: "10px"
                    },
                    {
                        bSortable: false,
                        aTargets: [1],
                        width: "100px"
                    },
                    {
                        aTargets: [3],
                        width: "50px"
                    },
                    {
                        bSortable: false,
                        aTargets: [4],
                        width: "100px",
                        "type": "date",
                    },
                    {
                        bSortable: false,
                        aTargets: [5],
                        width: "100px",
                        "type": "date",
                    },
                    {
                        bSortable: false,
                        aTargets: [6],
                        width: "150px"
                    }]
                });

                //executable.inserTheRow($table, datatable);
                $(".dropdown-toggle").dropdown();

                $table.on('click', 'i[data-toggle]', function () {
                    var $this = $(this),
                        tr = $(this).closest('tr').get(0);

                    if (datatable.fnIsOpen(tr)) {
                        $this.removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                        datatable.fnClose(tr);
                    } else {
                        $this.removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                        $(".dropdown-toggle").dropdown();
                        datatable.fnOpen(tr, Execution.fnFormatDetails(datatable, tr), 'details');
                        $(".dropdown-toggle").dropdown();
                    }
                });

                //console.log("datatable: ", datatable);
                //$('.dataTables_filter input', datatable.rows().nodes());

                //$(document).on("keyup", '.dataTables_filter input', function () {
                //    var extNo = $(this).val();
                //    console.log("text: ", extNo);
                //});

            },
            fnFormatDetails: function (datatable, tr) {
                var data = datatable.fnGetData(tr);

                return [
                    '<table class="table mb-none">',
                        '<tr class="b-top-none">',
                            '<td><label class="mb-none">Full Name:</label></td>',
                            '<td>' + data.FullName + '</td>',
                            '<td><label class="mb-none">Company Name:</label></td>',
                            '<td>' + data.CompanyName + '</td>',
                        '</tr>',
                        '<tr class="b-top-none">',
                            '<td><label class="mb-none">Email:</label></td>',
                            '<td>' + data.Email + '</td>',
                            '<td><label class="mb-none">Company Email:</label></td>',
                            '<td>' + data.CompanyEmail + '</td>',
                        '</tr>',
                        '<tr>',
                            '<td><label class="mb-none">Address:</label></td>',
                            '<td>' + data.Address + ' mins. </td>',
                            '<td><label class="mb-none">Company Address:</label></td>',
                            '<td>' + data.CompanyAddress + '</td>',
                        '</tr>',
                        '<tr>',
                            '<td><label class="mb-none">Phone Number:</label></td>',
                            '<td>' + data.PhoneNumber + '</td>',
                            '<td><label class="mb-none">Company Phone:</label></td>',
                            '<td>' + data.CompanyPhone + '</td>',
                        '</tr>',
                        '<tr>',
                            '<td><label class="mb-none">Mobile:</label></td>',
                            '<td>' + data.MobileNumber + '</td>',
                            '<td><label class="mb-none">Company Reg.No:</label></td>',
                            '<td>' + data.CompanyRegistrationNumber + '</td>',
                        '</tr>',
                         '<tr>',
                            '<td><label class="mb-none">DOB:</label></td>',
                            '<td>' + data.DOB + '</td>',
                            '<td><label class="mb-none">Date of Incorp. :</label></td>',
                            '<td>' + data.CompanyDateStarted + '</td>',
                        '</tr>',
                         '<tr>',
                            '<td><label class="mb-none">NID:</label></td>',
                            '<td>' + data.NIDNumber + '</td>',
                            '<td><label class="mb-none">Id:</label></td>',
                            '<td>' + data.Id + '</td>',
                        '</tr>',
                        '<tr>',
                            '<td><label class="mb-none">Passport No:</label></td>',
                            '<td>' + data.PassportNumber + '</td>',
                            '<td><label class="mb-none">Company Website.:</label></td>',
                            '<td><a href="' + data.CompanyWebsite + '" target="_blank">' + data.CompanyWebsite + '</a></td>',
                        '</tr>',
                        '<tr>',
                            '<td><label class="mb-none">Next Of Kin:</label></td>',
                            '<td>' + data.NextOFKin + '</td>',
                            '<td><label class="mb-none">Company Contact Person:</label></td>',
                            '<td>' + data.CompanyContactPerson + '</td>',
                        '</tr>',
                         '<tr>',
                            '<td><label class="mb-none">Mobile:</label></td>',
                            '<td>' + data.MobileNumber + '</td>',
                            '<td><label class="mb-none">Company Fax:</label></td>',
                            '<td>' + data.CompanyFax + '</td>',
                        '</tr>',
                        '<tr>',
                            '<td colspan="2" class="align-right">' +
                            "<a href='/Patient/Details/" + data.Id + "' class='btn btn-default' target='_blank'>&nbsp;<i class='fa fa-address-card'></i>&nbsp;Card</a>" +
                            "&nbsp;" + "<a href='/Provider/StartMeeting/" + data.Id + "'target='_blank' class='btn btn-primary'>&nbsp;<i class='fa fa-sign-in'></i>&nbsp;Create Account</a>" +
                            "&nbsp;" + "<a href='mailto:" + data.Email + "'target='_blank' class='btn btn-danger'>&nbsp;<i class='fa fa-envelope'></i>&nbsp;Email Contact</a>" +
                            //"&nbsp;" + executable.generateEncounterButtons(data.Id) + '</td>',
                        '</tr>',
                    '</table>'
                ].join('');
            },
        }
    })();

    Execution.Init();
});