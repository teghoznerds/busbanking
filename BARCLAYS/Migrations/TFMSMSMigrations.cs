﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using BARCLAYS.Models;

namespace BARCLAYS.Migrations
{
    public class BARCLAYSMigrations
    {
        ApplicationDbContext _context;
        public BARCLAYSMigrations(ApplicationDbContext context)
        {
            _context = context;            
        }
        public void DoUserMigration()
        {
            if (!(_context.BARCLAYSUsers.Any(u => u.Email == "super@infinigroup.biz")))
            {
                var userToInsert = new BARCLAYSUsers()
                {
                    Id = 0,
                    Email = "super@infinigroup.biz",
                    Username = "2555144",
                    FirstName = "Prem",
                    DOB = DateTime.Parse("1980-01-01"),
                    Code = "SUPER1",
                    isActive = true,
                    LastName = "Maddalli",
                    Telephone = "2555144",
                    DateLastUpdated = DateTime.Now,
                    DateCreated = DateTime.Now,
                    UserTypes = UserTypes.Admin                   
                };
                _context.BARCLAYSUsers.Add(userToInsert);

                _context.SaveChanges();

                _context.BARCLAYSAdmin.Add(new BARCLAYSAdmin
                {
                    BARCLAYSUserId = _context.BARCLAYSUsers.Where(cu => cu.Username == "2555144").FirstOrDefault().Id,                    
                });
            }

            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_context));

            string AdminRole = "Admin";
            string SuperUserRole = "Superuser";
            string CustomerRole = "Client";

            if (!RoleManager.RoleExists(AdminRole))
            {
                RoleManager.Create(new IdentityRole(AdminRole));
            }

            if (!RoleManager.RoleExists(SuperUserRole))
            {
                RoleManager.Create(new IdentityRole(SuperUserRole));
            }

            if (!RoleManager.RoleExists(CustomerRole))
            {
                RoleManager.Create(new IdentityRole(CustomerRole));
            }

            if (!(_context.Users.Any(u => u.UserName == "superuser")) && _context.BARCLAYSUsers.Count() > 0)
            {
                var userStore = new UserStore<ApplicationUser>(_context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var superUserId = _context.BARCLAYSUsers.Where(a => a.Email == "super@infinigroup.biz").FirstOrDefault()?.Id;
                //var superUserId = 0;
                var userToInsert = new ApplicationUser
                {
                    UserName = "2555144",
                    FirstName = "Prem",
                    LastName = "Maddalli",
                    PhoneNumber = "2555144",
                    Email = "super@infinigroup.biz",
                    IsActive = true,
                    BARCLAYSUserId = superUserId,
                };
                var result = userManager.Create(userToInsert, "BARCLAYS@2015");

                if (result.Succeeded)
                {
                    userManager.AddToRole(userToInsert.Id, "Admin");
                    userManager.AddToRole(userToInsert.Id, "Superuser");

                    var userId = _context.Users.Where(u => u.UserName == "2555144").FirstOrDefault().Id;
                    BARCLAYSUsers superUser =
                        _context.BARCLAYSUsers.Where(a => a.Email == "super@infinigroup.biz").FirstOrDefault();
                    superUser.UserId = userId;
                    _context.BARCLAYSUsers.AddOrUpdate(superUser);
                }

            }

            if (_context.Users.Any(u => u.UserName == "superuser") && _context.BARCLAYSUsers.Any(u => u.Email == "super@infinigroup.biz"))
            {
                var superUserId = _context.BARCLAYSUsers.Where(a => a.Email == "super@infinigroup.biz").FirstOrDefault()?.UserId;
                ApplicationUser user = _context.Users.Find(superUserId);
                user.BARCLAYSUserId = _context.BARCLAYSUsers.Where(a => a.Email == "super@infinigroup.biz").FirstOrDefault()?.Id;
                _context.Entry(user).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }
    }

}